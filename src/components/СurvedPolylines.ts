interface CurvedPolylinesOptions {
  latStart: number
  lngStart: number;
  latEnd: number;
  lngEnd: number;
  color?: string;
  opacity: number;
  horizontal?: number;
  weight?: number;
  gapWidth?: number;
  multiplier?: number;
  resolution?: number;
}

export default class CurvedPolylines {

  latStart: number;
  lngStart: number;
  latEnd: number;
  lngEnd: number;
  color: string = "#FF0000";
  weight: number = 2;
  opacity: number;
  horizontal: boolean = true;
  multiplier: number = 3.14;
  resolution: number = 0.07;


  constructor(options: CurvedPolylinesOptions) {
    this.latStart = options.latStart;
    this.lngStart = options.lngStart;
    this.latEnd = options.latEnd;
    this.lngEnd = options.lngEnd;
    this.color = options.color;
  }

  public setLines(map) {
    let lastLat = this.latStart;
    let lastLng = this.lngStart;

    let partLat;
    let partLng;

    let points = [];
    let pointsOffset = [];

    for (let point = 0; point <= 1; point += this.resolution) {
      points.push(point);
      pointsOffset.push(0.6 * Math.sin((Math.PI * point)));
    }

    let offsetMultiplier = 0;

    let offsetLength = this.horizontal == true
      ? (this.lngEnd - this.lngStart) * 0.1
      : (this.latEnd - this.latStart) * 0.1;


    for (let i = 0; i < points.length; i++) {

      if (i == 4)
        offsetMultiplier = 1.5 * this.multiplier;

      offsetMultiplier = (offsetLength * pointsOffset[i]) * this.multiplier;

      partLat = this.latStart + ((this.latEnd - this.latStart) * points[i]);
      partLng = this.lngStart + ((this.lngEnd - this.lngStart) * points[i]);
      this.horizontal == true
        ? partLat += offsetMultiplier
        : partLng += offsetMultiplier;

      this.curvedLineCreateSegment(lastLat, lastLng, partLat, partLng, map);

      lastLat = partLat;
      lastLng = partLng;
    }
    this.curvedLineCreateSegment(lastLat, lastLng, this.latEnd, this.lngEnd, map);
  }

  private curvedLineCreateSegment(latStart, lngStart, latEnd, LngEnd, map) {
    let lineCoordinates = [];

    lineCoordinates[0] = new google.maps.LatLng(latStart, lngStart);
    lineCoordinates[1] = new google.maps.LatLng(latEnd, LngEnd);

    let Line = new google.maps.Polyline({
      path: lineCoordinates,
      geodesic: false,
      strokeColor: this.color,
      strokeOpacity: this.opacity,
      strokeWeight: this.weight
    });

    Line.setMap(map);
  }
}


