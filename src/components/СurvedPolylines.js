"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CurvedPolylines = /** @class */ (function () {
    function CurvedPolylines(options) {
        this.color = "#FF0000";
        this.weight = 2;
        this.horizontal = true;
        this.multiplier = 3.14;
        this.resolution = 0.07;
        this.latStart = options.latStart;
        this.lngStart = options.lngStart;
        this.latEnd = options.latEnd;
        this.lngEnd = options.lngEnd;
        this.color = options.color;
    }
    CurvedPolylines.prototype.setLines = function (map) {
        var lastLat = this.latStart;
        var lastLng = this.lngStart;
        var partLat;
        var partLng;
        var points = [];
        var pointsOffset = [];
        for (var point = 0; point <= 1; point += this.resolution) {
            points.push(point);
            pointsOffset.push(0.6 * Math.sin((Math.PI * point)));
        }
        var offsetMultiplier = 0;
        var offsetLength = this.horizontal == true
            ? (this.lngEnd - this.lngStart) * 0.1
            : (this.latEnd - this.latStart) * 0.1;
        for (var i = 0; i < points.length; i++) {
            if (i == 4)
                offsetMultiplier = 1.5 * this.multiplier;
            offsetMultiplier = (offsetLength * pointsOffset[i]) * this.multiplier;
            partLat = this.latStart + ((this.latEnd - this.latStart) * points[i]);
            partLng = this.lngStart + ((this.lngEnd - this.lngStart) * points[i]);
            this.horizontal == true
                ? partLat += offsetMultiplier
                : partLng += offsetMultiplier;
            this.curvedLineCreateSegment(lastLat, lastLng, partLat, partLng, map);
            lastLat = partLat;
            lastLng = partLng;
        }
        this.curvedLineCreateSegment(lastLat, lastLng, this.latEnd, this.lngEnd, map);
    };
    CurvedPolylines.prototype.curvedLineCreateSegment = function (latStart, lngStart, latEnd, LngEnd, map) {
        var lineCoordinates = [];
        lineCoordinates[0] = new google.maps.LatLng(latStart, lngStart);
        lineCoordinates[1] = new google.maps.LatLng(latEnd, LngEnd);
        var Line = new google.maps.Polyline({
            path: lineCoordinates,
            geodesic: false,
            strokeColor: this.color,
            strokeOpacity: this.opacity,
            strokeWeight: this.weight
        });
        Line.setMap(map);
    };
    return CurvedPolylines;
}());
exports.default = CurvedPolylines;
//# sourceMappingURL=СurvedPolylines.js.map