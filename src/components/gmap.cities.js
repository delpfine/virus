export default [
  {
    id: 0,
    icon: '/static/img/markers/makger_blue.png',
    label: 'Москва',
    position: {
      lat: 55.755826,
      lng: 37.6172999
    }
  },
  {
    id: 1,
    icon: '/static/img/markers/makger_blue.png',
    label: 'Ногинск',
    position: {
      lat: 55.8761163,
      lng: 38.4666594
    }
  },
  {
    id: 2,
    icon: '/static/img/markers/makger_green.png',
    label: 'Истра',
    position: {
      lat: 55.9230933,
      lng: 36.8660655
    }
  },
  {
    id: 3,
    icon: '/static/img/markers/makger_blue.png',
    label: 'Дмитров',
    position: {
      lat: 56.3427702,
      lng: 37.5288416
    }
  },
  {
    id: 3,
    icon: '/static/img/markers/makger_green.png',
    label: 'Клин',
    position: {
      lat: 56.3333816,
      lng: 36.730447
    }
  },
  {
    id: 4,
    icon: '/static/img/markers/makger_green.png',
    label: 'Фрязино',
    position: {
      lat: 55.9602597,
      lng: 38.0441437
    }
  },
 /* {
    id: 4,
    icon: '/static/img/markers/makger_green.png',
    label: 'Волоколамск',
    position: {
      lat:56.0363252,
      lng:35.9573132
    }
  }*/
];
